fish_config theme choose "Catppuccin Mocha"

if status is-interactive
    # Commands to run in interactive sessions can go here
end

set fish_greeting

starship init fish | source

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first --icons' # my preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.='exa -a | egrep "^\."'
alias update-grub='sudo grub-mkconfig -o /boot/grub/grub.cfg' #updating grub config

#first line removes the path and second line makes it so fish isn't slow loading user paths
set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths $HOME/.emacs.d/bin $HOME/.cargo/bin
#set -Ux QT_QPA_PLATFORMTHEME qt5ct 

#Use vim keybindings
function fish_user_key_bindings
 fish_vi_key_bindings
end

# The bindings for !! and !$

function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

#ColorScripts
#colorscript -r

#Custom Neofetch
#neofetch --ascii_distro debian_small
