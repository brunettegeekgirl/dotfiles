# Created by Randa Kerbache for 5.9

# History configuration #
#-----------------------#

# history file location
HISTFILE=~/.cache/zsh/history
# hisotry file length
HISTSIZE=10000
SAVEHIST=10000
# immediate append
setopt INC_APPEND_HISTORY
export HISTTIMEFORMAT="[%F %T]"
# add Timestamp to history
setopt EXTENDED_HISTORY
# disable duplication
setopt HIST_FIND_NO_DUPS

# If not running interactively, don't do anything #
#-------------------------------------------------#

[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# Use Vim as editor in terminal #
#-------------------------------#

export EDITOR="vim"
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=5"

# Changing "ls" to "exa" #
#------------------------#

alias ls='exa -al --color=always --group-directories-first --icons' # my preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.='exa -a --icons | egrep "^\."'

# Git bare #
#----------#

alias mygit='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# Colorscript #
#-------------#

# colorscript --random

# Autocompletion & Syntax highlighting #
#--------------------------------------#

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Starship #
#----------#

eval "$(starship init zsh)"

# My custom Neofetch #
#--------------------#

#neofetch --ascii_distro arch_small
