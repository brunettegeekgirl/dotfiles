<h3>Welcome to my Dotfiles repo!</h3>

In here, I'm going to manage my dotfiles/configs for the different tools I use.

I'm currently running <b>Arch</b> with the <b>XFCE4</b> DE.

<p align="center"><img src="https://i.servimg.com/u/f34/11/28/65/59/screen11.jpg" alt="My Desktop"/></p>

<p align="center"><img src="https://i.servimg.com/u/f34/11/28/65/59/screen11.png" alt="My Desktop"/></p>

**Details:**
- Login/Display manager: LightDM GTK Greeter with the Flat Remix Teal Dark theme/icons and Noto Kufi Arabic Regular fonts 
- Filesystem: btrfs with zram
- Shell: zsh
- DE: XFCE4 (4.18)
- System fonts: Sans Regular
- Theme: Nordic
- Icon theme: Papirus-Dark + Papirus Folders bluegrey
- Cursor theme: Nordic-cursors
- Specific panel widgets: Whisker menu - workspace switcher - AppMenu plugin (Vala panel) - Redshift GTK
- Terminal: XFCE4 terminal
- Terminal prompt: Starship
- Terminal font: FiraCode NF Regular
- Terminal colorscheme: Nord
