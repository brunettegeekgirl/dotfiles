"Make the vim editor display line number by default with other lines relatively to current one
set number relativenumber
"Change gutter column width for numbering in vim
set numberwidth=2
"Configure text wrapping for the number column in vim
set cpoptions+=n
"Setup syntaxe highliting
syntax enable
"Setup highlighting search results
set hls is
