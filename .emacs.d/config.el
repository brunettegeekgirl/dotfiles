(defvar elpaca-installer-version 0.5)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
			      :ref nil
			      :files (:defaults (:exclude "extensions"))
			      :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
	(if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
		 ((zerop (call-process "git" nil buffer t "clone"
				       (plist-get order :repo) repo)))
		 ((zerop (call-process "git" nil buffer t "checkout"
				       (or (plist-get order :ref) "--"))))
		 (emacs (concat invocation-directory invocation-name))
		 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
				       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
		 ((require 'elpaca))
		 ((elpaca-generate-autoloads "elpaca" repo)))
	    (progn (message "%s" (buffer-string)) (kill-buffer buffer))
	  (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable :elpaca use-package keyword.
  (elpaca-use-package-mode)
  ;; Assume :elpaca t unless otherwise specified.
  (setq elpaca-use-package-by-default t))

;; Block until current queue processed.

(elpaca-wait)

(use-package evil
  :init
  (setq evil-want-keybinding nil
	evil-want-C-i-jump nil
	evil-undo-system 'undo-redo
	evil-search-module 'evil-search
	evil-vsplit-window-right t
	evil-split-window-below t)
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package general
  :config
  (general-evil-setup)
  (general-create-definer doom
			  :states '(normal insert visual emacs)
			  :keymaps 'override
			  :prefix "SPC"
			  :global-prefix "M-SPC")

;; For buffers
(doom
 "<left>" '(previous-buffer :wk "Previous buffer")
 "<right>" '(next-buffer :wk "Next buffer")
 "b" '(:ignore t :wk "Buffer keys")
 "bk" '(kill-this-buffer :wk "Kill buffer")
 "bs" '(switch-to-buffer :wk "Switch buffer"))
;; For files
(doom
 "." '(find-file :wk "Find file")
 "f" '(:ignore t :wk "File keys")
 "fc" '((lambda () (interactive) (find-file "~/.emacs.d/config.org")) :wk "Find Emacs config file")))

(set-face-attribute 'default nil
  :font "Hack"
  :height 110
  :weight 'normal)
(set-face-attribute 'variable-pitch nil
  :font "FiraCode Nerd Font"
  :height 110
  :weight 'normal)
(set-face-attribute 'fixed-pitch nil
  :font "Hack"
  :height 110
  :weight 'normal)

(add-to-list 'default-frame-alist '(font . "Hack-11"))

(electric-indent-mode -1)
(setq org-edit-src-content-indentation 0)

(use-package org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(add-hook 'org-mode-hook 'org-indent-mode)

(require 'org-tempo)

(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-nord-aurora t)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-visual-line-mode)
(global-display-line-numbers-mode)
(setq display-line-numbers 'relative)

(use-package which-key
  :init
  (setq which-key-idle-delay 0.5)
  (which-key-mode 1)
  :config
  (setq which-key-side-window-location 'bottom
	which-key-sort-order #'which-key-prefix-then-key-order
	which-key-allow-imprecise-window-fit nil
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 7
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.35
	which-key-max-description-length 35
	which-key-show-remaining-keys t
	which-key-prefix-prefix "• "
	which-key-separator " │ " ))
