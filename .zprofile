# Created by Randa Kerbache #

# User directories #
#------------------#

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

# System directories #
#--------------------#

export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_CONFIG_DIRES="/etc/xdg"
export ZDOTDIR="$HOME/.config/zsh"
export STARSHIP_CONFIG="$HOME/.config/starship.toml"
export HISTFILE="$XDG_CACHE_HOME"/zsh/history

# .local/bin path #
#-----------------#

export PATH="$HOME/.local/bin:$HOME/.cargo/env:$PATH"
